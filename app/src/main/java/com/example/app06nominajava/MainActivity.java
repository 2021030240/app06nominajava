package com.example.app06nominajava;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnEntrar;
    private EditText txtNombre;
    private Button btnSalir;
    private void iniciar()
    {
        txtNombre = findViewById(R.id.txtNombre);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
    }
    private void aceptar(){finish();}
    private void setBtnCerrar()
    {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("¿CERRAR APP?");
        confirmar.setMessage("Se descartara Toda Informacion Ingresada");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {aceptar();}
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

            }
        });
        confirmar.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciar();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validar())
                {
                    Bundle bundle = new Bundle();
                    bundle.putString("Nombre", txtNombre.getText().toString());

                    Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else{
                    Toast.makeText(getApplicationContext(), "Faltó capturar datos ",Toast.LENGTH_SHORT).show();
                    txtNombre.requestFocus();
                }
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBtnCerrar();
            }
        });
    }
    private boolean validar()
    {
        boolean exito = true;
        Log.d("Nombre", "validar: "+ txtNombre.getText());
        if(txtNombre.getText().toString().equals("")) exito = false;
        return exito;
    }
}