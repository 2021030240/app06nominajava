package com.example.app06nominajava;

public class ReciboNomina
{
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    public  ReciboNomina()
    {
        this.numRecibo = 0;
        this.nombre = "";
        this.horasTrabNormal = 0.0f;
        this.horasTrabExtras = 0.0f;
        this.puesto = 0;
        this.impuestoPorc = 0.0f;
    }
    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc)
    {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }
    public ReciboNomina(ReciboNomina reciboNomina)
    {
        this.numRecibo = reciboNomina.numRecibo;
        this.nombre = reciboNomina.nombre;
        this.horasTrabNormal = reciboNomina.horasTrabNormal;
        this.horasTrabExtras = reciboNomina.horasTrabExtras;
        this.puesto = reciboNomina.puesto;
        this.impuestoPorc = reciboNomina.impuestoPorc;
    }

    public int getNumRecibo() {
        return numRecibo;
    }
    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }
    public String getNombre(){ return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
    public float getHorasTrabNormal(){ return horasTrabNormal; }
    public void setHorasTrabNormal( float horasTrabNormal) { this.horasTrabNormal = horasTrabNormal; }
    public float getHorasTrabExtras(){ return horasTrabExtras; }
    public void setHorasTrabExtras( float horasTrabExtras) { this.horasTrabExtras = horasTrabExtras; }
    public int getPuesto() {
        return puesto;
    }
    public void setPuesto(int puesto) { this.puesto = puesto; }
    public float getImpuestoPorc(){ return impuestoPorc; }
    public void setImpuestoPorc( float impuestoPorc) { this.impuestoPorc = impuestoPorc; }


    float calcularSubtotal()
    {
        float subtotal = 0, pNormal = 0, pExtra = 0;
        if(this.puesto == 1)
        {
            pNormal = 50;
            pExtra = 100;
        }
        else if(this.puesto == 2)
        {
            pNormal = 70;
            pExtra = 140;
        }
        else
        {
            pNormal = 100;
            pExtra = 200;
        }
        subtotal = pNormal * this.horasTrabNormal + pExtra * this.horasTrabExtras;
        return subtotal;
    }
    public float calcularImpuesto()
    {
        float impuesto = calcularSubtotal();
        impuesto = impuesto * 0.16f;
        return impuesto;
    }
    public float calcularTotal()
    {
        float total = calcularSubtotal() - calcularImpuesto();
        return total;
    }
}
