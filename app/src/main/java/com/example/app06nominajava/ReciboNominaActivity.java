package com.example.app06nominajava;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {

    private Button btnRegresar;
    private EditText txtNombre;
    private EditText txtNumeroRe;
    private EditText txtHorasN;
    private EditText txtHorasE;
    private TextView lblNombre;
    private Button btnLimpiar;
    private Button btnCalcular;
    private ReciboNomina recibo;
    private RadioButton rbtnAuxiliar;
    private RadioButton rbtnAlbañil;
    private RadioButton rbtnIng;
    private TextView lblSubtotal;
    private TextView lblImpuesto;
    private TextView lblTotal;
    private void iniciar()
    {
        btnRegresar = findViewById(R.id.btnRegresar);
        txtNombre = findViewById(R.id.txtNombre);
        lblNombre = findViewById(R.id.lblNombre);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCalcular = findViewById(R.id.btnCalcular);
        txtHorasE = findViewById(R.id.txtHorasExtras);
        txtHorasN = findViewById(R.id.txtHorasNormales);
        txtNumeroRe = findViewById(R.id.txtNumRecibo);
        rbtnAuxiliar = findViewById(R.id.rbtnAuxiliar);
        rbtnAlbañil = findViewById(R.id.rbtnAlbañil);
        rbtnIng = findViewById(R.id.rbtnIng);
        lblImpuesto = findViewById(R.id.lblImpuesto);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblTotal = findViewById(R.id.lblTotal);
    }
    private void btnLimpiar()
    {
        txtNumeroRe.setText("");
        txtNombre.setText("");
        txtHorasN.setText("");
        txtHorasE.setText("");
        rbtnAlbañil.setChecked(false);
        rbtnIng.setChecked(false);
        rbtnAuxiliar.setChecked(false);
        lblSubtotal.setText("Subtotal: ");
        lblImpuesto.setText("Impuesto: ");
        lblTotal.setText("Total: ");

        recibo = new ReciboNomina();
    }
    private void btnCalcular()
    {
        int puesto = 1;
        if(rbtnAlbañil.isChecked())
        {
            puesto = 2;
        }
        else if (rbtnIng.isChecked())
        {
            puesto = 3;
        }
        recibo = new ReciboNomina(
            Integer.parseInt(txtNumeroRe.getText().toString()),
            txtNombre.getText().toString(),
            Integer.parseInt(txtHorasN.getText().toString()),
            Integer.parseInt(txtHorasE.getText().toString()),
            puesto,
0
        );
        float subtotal = recibo.calcularSubtotal();
        float impuesto = recibo.calcularImpuesto();
        float total = recibo.calcularTotal();

        lblTotal.setText("Total:"+ total + "$");
        lblImpuesto.setText("Impuesto:" + impuesto + "$");
        lblSubtotal.setText("Subtotal:" + subtotal + "$");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);
        iniciar();
        Bundle recibeD = getIntent().getExtras();
        String nombre = recibeD.getString("Nombre");
        lblNombre.setText("Empleado: " + nombre);
        txtNombre.setText(nombre);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLimpiar();
            }
        });
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validar())
                {
                    btnCalcular();
                } else{
                    Toast.makeText(getApplicationContext(), "Faltó capturar datos ",Toast.LENGTH_SHORT).show();
                    txtNumeroRe.requestFocus();
                }
            }
        });
    }
    private boolean validar()
    {
        boolean exito = true;
        Log.d("Nombre", "validar: "+ txtNombre.getText());
        if(txtNumeroRe.getText().toString().equals("")) exito = false;
        if(txtNombre.getText().toString().equals("")) exito = false;
        if(txtHorasE.getText().toString().equals("")) exito = false;
        if(txtHorasN.getText().toString().equals("")) exito = false;
        if(rbtnIng.isChecked() || rbtnAlbañil.isChecked() || rbtnAuxiliar.isChecked())exito = true; else exito = false;
        return exito;
    }
}